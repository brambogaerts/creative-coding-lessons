// Find the plane and the debug divs
const debug = document.querySelector('.debug');
const plane = document.querySelector('.plane');

// Check whether the ontouchstart event exists, if it does, we're probably on a mobile device. Using a ternary operator here, look it up in the glossary.
const isMobileDevice = 'ontouchstart' in document ? true : false;

if (isMobileDevice) {
	// If we're on a mobile device, show the debug screen
	debug.classList.add('visible');
} else {
	// Otherwise, show the plane
	plane.classList.add('visible');
}

// Prevent default touch behaviour (scrolling, etcetera)
window.addEventListener('touchstart', function(e) {
	e.preventDefault();
}, {
	passive: false
});

// Connect to the socket server
const socket = io();

// Listen for the touch move event
window.addEventListener('touchmove', function(e) {
	// Normalize the x and y values of the first touch in the list to fall within the range 0-1
	let x = e.touches[0].clientX / window.innerWidth;
	let y = e.touches[0].clientY / window.innerHeight;

	// Show the debug information in the debug div
	debug.innerHTML =  '<b>x</b> ' + x + '<br/>' + '<b>y</b> ' + y;
	
	// Send the normalized touch data through the socket to the server
	socket.emit('touch', {x: x, y: y});
});

// Listen for the end of a touch
window.addEventListener('touchend', function(e) {
	// Show the debug information in the debug div
	debug.innerHTML =  'Touch your phone to start';
	
	// Send the release event to the server
	socket.emit('release');
});

// Listen for the 'move' event that our server sends us
socket.on('move', function(data) {
	// Transform the incoming data (range 0-1) to a range of -90 to 90 degrees
	let rotationOnYAxis = (data.x  - 0.5) * 90 + 'deg';
	let rotationOnXAxis = (data.y  - 0.5) * -90 + 'deg';

	// Set the transform property of the plane and disable transitions
	plane.style.transform = 'translate(-50%, -50%) rotateX(' + rotationOnXAxis + ') rotateY(' + rotationOnYAxis + ')';
	plane.style.transition = 'none';
});

// Listen for the 'center' event that our server sends us
socket.on('center', function() {
	debug.innerHTML =  'Touch your phone to start';

	// Set the transform property of the plane and enable transitions
	plane.style.transform = 'translate(-50%, -50%)';
	plane.style.transition = 'transform .3s ease';
	
});
