# Creative Coding Lessons: WebSockets

This is the source code for the lesson on WebSockets.

## How to run

1. Clone this repository.
2. In a terminal, navigate to this folder. For example: `cd ~/Development/creative-coding-lessons/WebSockets`
3. Run `node .`
4. Open a browser and point it to `localhost:8080`
5. On your phone, open a browser and point it to the IP address of your computer and add the port. For example: `192.168.128.202:8080`
