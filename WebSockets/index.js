// Import Express, Socket.IO and the built-in HTTP module
const express = require('express');
const socketIO = require('socket.io');
const http = require('http');

// Set up our app
const app = express();

// Create a variable that contains a reference to the location of our static files, in this case it's the folder called 'app'
const staticFolder = express.static('app');

// Tell our app to use the static folder we referenced above
app.use(staticFolder);

////
// Extract the http server
const server = http.createServer(app);

// Bind Socket.IO to our server
const io = socketIO(server);
////

// Listen for socket connections
io.on('connection', function(socket) {
	// Listen for the touch event coming from the phone
	socket.on('touch', function(data) {
		// When the touch event fires, emit the move event to the other clients
		io.emit('move', data);
	});

	socket.on('release', function() {
		// When the touch event fires, emit the move event
		io.emit('center');
	});
});

// Finally, we tell the app to listen on port 8080
// app.listen(8080);

////
// Finally, we tell the server to listen on port 8080
server.listen(8080);
////
